package me.landrade.src;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Portal extends Anim {
	public Portal(float posX, float posY, World w) {
		super("anim/portal.png", 10, 50);
		this.pos(posX, posY).world(w).size(120, 79).sSize(120, 79).name("Portal");
	}
	
	@Override
	public TextureRegion frameAt(int index) {
		return new TextureRegion(sheet, index * 120, 0, 120, 79);
	}
}