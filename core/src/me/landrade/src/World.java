package me.landrade.src;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class World {
	protected Texture worldBg;
	protected SpriteBatch batch = new SpriteBatch();
	protected String name;
	protected int limit = 16;
	
	public ArrayList<Dove> doves = new ArrayList<Dove>();
	public ArrayList<Anim> animations = new ArrayList<Anim>();
	
	public World init() { return this; }
	public void update(float delta) {}
	public void render(float delta) {}
	public int getLivingDoves() {
		int n = 0;
		for(int x = 0 ; x < doves.size() ; x++) {
			if(doves.get(x).alive)
				n++;
		}
		return n;
	}
	
	public static ArrayList<Anim> garbageA = new ArrayList<Anim>();
	public static ArrayList<Dove> garbageD = new ArrayList<Dove>();
	public void clearGarbage() {
		garbageA.removeAll(garbageA);
		garbageA.addAll(animations);
		animations.removeAll(animations);
		for(Anim e : garbageA) {
			if(e.alive)
				animations.add(e);
		}
		garbageA.removeAll(garbageA);
		
		garbageD.removeAll(garbageD);
		garbageD.addAll(doves);
		doves.removeAll(doves);
		for(Dove e : garbageD) {
			if(e.alive)
				doves.add(e);
		}
		garbageD.removeAll(garbageA);
	}
	
	public boolean isAnyDragging() {
		for(int x = 0 ; x < doves.size() ; x++) {
			if(doves.get(x).isDragging)
				return true;
		}
		return false;
	}
}
