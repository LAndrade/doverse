package me.landrade.src;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Fonts {
	
	public static BitmapFont fixedsys;
	
	public static void init() {
		fixedsys = new BitmapFont(Gdx.files.internal("fonts/fixedsys.fnt"));
	}
	
}
