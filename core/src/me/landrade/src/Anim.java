package me.landrade.src;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class Anim {
	int currentIndex = 0;
	int maxIndex = 8;
	boolean alive = true;
	float posX = 100;
	float posY = 100;
	float width = 100;
	float height = 100;
	float sWidth = 100;
	float sHeight  = 100;
	float speed;
	String uri;
	String s;
	Texture sheet;
	TextureRegion[] frames;
	World w;
	boolean firstRun = true;
	
	public Anim(String uri, int frames, float speed) {
		this.uri = uri;
		this.maxIndex = frames;
		this.speed = speed;
	}
	
	public Anim pos(float posX, float posY) {
		this.posX = posX;
		this.posY = posY;
		return this;
	}
	
	public Anim world(World w) {
		this.w = w;
		return this;
	}
	
	public Anim size(float w, float h) {
		this.width = w;
		this.height =  h;
		return this;
	}
	
	public Anim sSize(float w, float h) {
		this.sWidth = w;
		this.sHeight = h;
		return this;
	}
	
	public Anim name(String s) {
		this.s = s;
		return this;
	}
	
	public void update(float delta) {
		if(firstRun) {
			sheet = new Texture(uri);
			frames = new TextureRegion[maxIndex];
			for(int x = 0 ; x < maxIndex ; x++) {
				frames[x] = frameAt(x);
			}
			new Handler().start();
			firstRun = false;
		}

		if(alive)
			render(delta);
	}

	public TextureRegion frameAt(int index) {
		return new TextureRegion(sheet, index * sWidth, 0, sWidth, sHeight);
	}
	
	public void render(float delta) {
		w.batch.begin();
		w.batch.draw(frames[currentIndex], posX, posY, width, height);
		w.batch.end();
		if(Game.DEBBUGING) {
			Game.sRenderer.begin(ShapeType.Line);
			Game.sRenderer.setColor(Color.GREEN);
			Game.sRenderer.rect(posX, posY, width, height);
			Game.sRenderer.end();
		}
	}

	public class Handler extends Thread {
		public void run() {
			while(alive) {
				try {
					update();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		@SuppressWarnings("static-access")
		public void update() throws InterruptedException {
			if(currentIndex < maxIndex - 1) {
				currentIndex++;
				this.sleep((long) speed);
			} else {
				currentIndex = 0;
				alive = false;
				this.interrupt();
			}
		}
	}
	
	public String getName() {
		return s;
	}
}
