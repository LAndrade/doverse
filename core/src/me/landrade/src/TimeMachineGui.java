package me.landrade.src;

import me.landrade.src.Game.Mouse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class TimeMachineGui extends Gui {
	
	Texture bg;
	Texture marker;
	Sprite wheel; 
	CloseButton cButton;
	TMButton button;
	WheelButton w;
	TravelAnimation t;
	float dt;
	
	public void init() {
		batch = new SpriteBatch();
		bg = new Texture("ngui/timeMachine.png");
		marker = new Texture("ngui/tmMarker.png");
		wheel = new Sprite(new Texture("ngui/tmWheel.png"));
		wheel.setPosition(88, 331);
		wheel.setSize(245, 245);
		wheel.setOrigin(wheel.getWidth() / 2, wheel.getHeight() / 2);
		batch.setProjectionMatrix(Game.camera.combined);
		button = new TMButton();
		button.init();
		cButton = new CloseButton(189, 115);
		t = new TravelAnimation();
		w = new WheelButton();
		w.init();
	}
	
	public void update(float delta) {
		if(wheel.getRotation() > 360 || wheel.getRotation() < -360)
			wheel.setRotation(0);
		
		if(Mouse.down() && wheel.getBoundingRectangle().overlaps(Mouse.rect())) {
			if(Mouse.getY() > wheel.getY() + (wheel.getHeight() / 2))
				wheel.setRotation(wheel.getRotation() + (Gdx.input.getDeltaX() * -1) / 2);
			else
				wheel.setRotation(wheel.getRotation() + (Gdx.input.getDeltaX()) / 2);
		}
		
		render(delta);
		
		if(!t.alive) {
			cButton.update();
			button.update();
			w.update();
		}
		t.update();
	}

	float size = 0, middle = Game.vWidth / 2, pos = 0;
	public void render(float delta) {
		batch.begin();
		batch.draw(guibg, 0, 0);
		batch.draw(bg, 0, 0);
		wheel.draw(batch);
		batch.draw(marker, 192, 556);
		size = Fonts.fixedsys.getBounds(getWorld(wheel.getRotation() + 6).name + " Age").width;
		pos = middle - size / 2;
		Fonts.fixedsys.draw(batch, getWorld(wheel.getRotation() + 6).name + " Age", pos, 650);
		batch.end();
	}
	
	public static World getWorld(float f) {
		if(f > 360)
			f = 360;
		if(f < -360)
			f = -360;
		
		if(f >= 0 && f <= 60 || f < -300 && f >= -360) // Selvagem
			return Game.wildWorld;
		if(f > 60 && f <= 120 || f < -240 && f >= -300) // Tribal
			return Game.tribalWorld;
		if(f > 120 && f <= 180 || f < -180 && f >= -260) // Medieval
			return Game.medievalWorld;
		if(f > 180 && f <= 240 || f < -120 && f >= -180) // Contemporaneo
			return Game.contemporaryWorld;
		if(f > 240 && f <= 300 || f < -60 && f >= -120) // Futurista
			return Game.futureWorld;
		if(f > 300 && f <= 360 || f < 0 && f >= -60) // Marinho
			return Game.waterWorld;
		return null;
	}
	
	public class TMButton {
		Texture texture;
		float posX = 55;
		float posY = 180;
		
		public void init() {
			texture = new Texture("ngui/tmButton.png");
		}
		
		public void update() {
			batch.begin();
			batch.draw(texture, posX, posY);
			batch.end();
			
			if(Mouse.down() && Mouse.rect().overlaps(bounds())) {
				Game.setWorld(getWorld(wheel.getRotation()));
				t.init(batch);
			}
		}
		
		public Rectangle bounds() {
			return new Rectangle(posX, posY, texture.getWidth(), texture.getHeight());
		}
	}
	
	public class WheelButton {
		Texture texture;
		float posX = 55;
		float posY = 250;
		
		public void init() {
			texture = new Texture("ngui/tmRotateButton.png");
		}
		
		public void update() {
			batch.begin();
			batch.draw(texture, posX, posY);
			batch.end();
			
			if(Mouse.down() && Mouse.rect().overlaps(bounds())) {
				wheel.setRotation(wheel.getRotation() + 3f);
			}
		}
		
		public Rectangle bounds() {
			return new Rectangle(posX, posY, texture.getWidth(), texture.getHeight());
		}
	}
	
	public class TravelAnimation {
		SpriteBatch s;
		boolean alive = false;
		float base;
		Sprite portal;
		ShapeRenderer r;
		
		public void init(SpriteBatch s) {
			this.s = s;	
			r = new ShapeRenderer();
			r.setProjectionMatrix(Game.camera.combined);
			portal = new Sprite(new Texture("portal.png"));
			portal.setPosition(0, 190);
			portal.setOriginCenter();
			alive = true;
		}
		
		public void update() {
			if(alive) {
				base += 0.5f;
				
				r.begin(ShapeType.Filled);
				r.setColor(Color.BLACK);
				r.rect(0, 0, Game.vWidth, Game.vHeight);
				r.end();
				
				portal.setScale(portal.getScaleX() * 1.02f, portal.getScaleY() * 1.02f);
				
				batch.begin();
				portal.draw(s);
				batch.end();
				
				portal.setRotation(portal.getRotation() + base);
				
				if(base > 50) {
					alive = false;
					base = 0;
				} 
			}
		}
	}
}
