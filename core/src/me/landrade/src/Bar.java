package me.landrade.src;

import me.landrade.src.Game.Mouse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class Bar {
	
	public Texture texture;
	SpriteBatch batch;
	public BarButton storeButton = new BarButton(358, 0, 63, 54, "ngui/phoneButton.png", Gui.phoneGui);
	public float width = 420;
	public float height = 115;
	public float posX = 0;
	public float posY = 0;
	
	public Bar() {
		texture = new Texture("ngui/bar.png");
		batch = Game.currentWorld.batch;
	}
	
	public void update(float delta) {
		render(delta);
		storeButton.update(delta); 
	}
	
	public void render(float delta) {
		batch.begin();
		batch.draw(texture, posX, posY, width, height);
		batch.end();
		if(Game.DEBBUGING) {
			Game.sRenderer.begin(ShapeType.Line);
			Game.sRenderer.setColor(Color.CYAN);
			Game.sRenderer.rect(posX, posY, width, height);
			Game.sRenderer.end();
		}
	}
	
	public class BarButton {
		float posX;
		float posY;
		float width;
		float height;
		
		String textureUri;
		Texture texture;
		Gui target;
		
		public BarButton(float posX, float posY, float width, float height, String texture, Gui target) {
			this.posX = posX;
			this.posY = posY;
			this.width = width;
			this.height = height;
			this.textureUri = texture;
			this.target = target;
			this.texture = new Texture(textureUri);
		}
		
		public void update(float delta) {
			if(Gdx.input.isTouched() && Mouse.rect().overlaps(bounds())) 
				onClick();
			render(delta);
		}
		
		public void render(float delta) {
			batch.begin();
			batch.draw(texture, posX, posY, width, height);
			batch.end();
		}
		
		public void onClick() {
			if(target != null)
				target.open();
			Game.out("Clicked");
		}
		
		public Rectangle bounds() {
			return new Rectangle(posX, posY, width, height);
		}
	}
}
