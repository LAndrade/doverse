package me.landrade.src;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;

public class WaterWorld extends World {

	Deployer[] d;
	Random rand = new Random();
	Handler h;

	public WaterWorld(String string) {
		this.name = string;
	}

	public World init() {
		batch.setProjectionMatrix(Game.camera.combined);
		worldBg = new Texture("worlds/waterWorld.png");
		return this;
	}

	public void initDoves() {
	}

	public void initDeployers() {
		d = new Deployer[4]; 
		d[0] = new Deployer(this, 20, 550).initDeployer();
		d[1] = new Deployer(this, 290, 410).initDeployer();
		d[2] = new Deployer(this, 150, 200).initDeployer();
		d[3] = new Deployer(this, 320, 650).initDeployer();
		h = new Handler();
		h.start();
	}

	public void update(float delta) {
		render(delta);
		for(int x = 0 ; x < 4 ; x++) 
			d[x].update(delta);
		
		for(int x = 0 ; x < doves.size() ; x++) 
			doves.get(x).update(delta);
		
		for(Anim e : animations) 
			e.update(delta);
		
		Dove d1, d2;
		for(int x = 0 ; x < doves.size() ; x++) {
			for(int y = 0 ; y < doves.size() ; y++) {
				d1 = doves.get(x);
				d2 = doves.get(y);
				if(d1 != d2 && d1.fuseBounds().overlaps(d2.fuseBounds()))
					d1.fuse(d1, d2);
			}
		}
		
		clearGarbage();
	}

	public void render(float delta) {
		batch.begin();
		batch.draw(worldBg, 0, 0);
		batch.end();
	}

	int dp = 0;
	int ld = -1;
	public class Handler extends Thread {
		public void run() {
			while(true) {
				try {
					if(Game.currentWorld == Game.waterWorld)
						update();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		@SuppressWarnings("static-access")
		public void update() throws InterruptedException {
			d[getDeployer(ld)].anim.deploy();
			this.sleep(5000);
		}
	}
	
	int x = 0;
	public int getDeployer(int ld) {
		x = rand.nextInt(d.length);
		if(x == ld)
			getDeployer(ld);
		else
			return x;
		return -1;
	}
}
