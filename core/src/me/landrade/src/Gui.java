package me.landrade.src;

import me.landrade.src.Game.Mouse;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Gui {
	protected boolean opened;
	protected SpriteBatch batch;
	protected int anchor = 800;
	protected static Texture guibg;
	public static final Gui timeMachine = new TimeMachineGui();
	public static final Gui phoneGui = new PhoneGui();
	
	public static void guiInit() {
		guibg = new Texture("ngui/guibg.png");
	}
	
	public void open() {
		Game.currentGui = this;
		opened = true; 
	};
	
	public void close() { opened = false; };
	public void init() {};
	public void update(float delta) {};
	public void render(float delta) {};
	public boolean isOpened() { return opened; };
	
	public class CloseButton {
		Texture img;
		float posX;
		float posY;
		float w = 30;
		float h = 30;
		
		public CloseButton(float posX, float posY) {
			this.posX = posX;
			this.posY = posY;
			img = new Texture("ngui/close.png");
		}
		
		public void setSize(float w, float h) {
			this.w = w;
			this.h = h;
		}
		
		public void update() {
			batch.begin();
			batch.draw(img, posX, posY, w, h);
			batch.end();
			
			if(Mouse.rect().overlaps(bounds()) && Mouse.down() && opened)
				close();
		}
		
		public Rectangle bounds() {
			return new Rectangle(posX, posY, w, h);
		}
	}
}
