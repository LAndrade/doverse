package me.landrade.src;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Explosion extends Anim {
	public Explosion(float posX, float posY, World w) {
		super("anim/explosion.png", 8, 50);
		this.pos(posX, posY).world(w).size(100, 100).name("Explosion");
	}
	
	@Override
	public TextureRegion frameAt(int index) {
		return new TextureRegion(sheet, index * 64, 0, 64, 64);
	}
}