package me.landrade.src;

import com.badlogic.gdx.graphics.Texture;

public class FutureWorld extends World {
	
	public FutureWorld(String string) {
		this.name = string;
	}
	
	public World init() {
		batch.setProjectionMatrix(Game.camera.combined);
		worldBg = new Texture("worlds/futureWorld.png");
		return this;
	}
	
	public void initDoves() {}
	
	public void update(float delta) {
		render(delta);
		for(int x = 0 ; x < doves.size() ; x++) 
			doves.get(x).update(delta);
		
		for(Anim e : animations) 
			e.update(delta);
		
		Dove d1, d2;
		for(int x = 0 ; x < doves.size() ; x++) {
			for(int y = 0 ; y < doves.size() ; y++) {
				d1 = doves.get(x);
				d2 = doves.get(y);
				if(d1 != d2 && d1.fuseBounds().overlaps(d2.fuseBounds()))
					d1.fuse(d1, d2);
			}
		}
		
		clearGarbage();
	}
	
	public void render(float delta) {
		batch.begin();
		batch.draw(worldBg, 0, 0);
		batch.end();
	}
}
