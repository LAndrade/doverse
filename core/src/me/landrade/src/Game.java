package me.landrade.src;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class Game extends ApplicationAdapter {

	public static int vWidth = 420;
	public static int vHeight = 800;
	public static int sWidth = 0;
	public static int sHeight = 0;
	public static OrthographicCamera camera;
	public static boolean dragging = false;
	public static World currentWorld;
	public static Gui currentGui;
	public static SpriteBatch gameBatch;
	public static boolean foreground = true;
	public static Bar bar;
	public static ShapeRenderer sRenderer;

	/* Worlds */
	public static World waterWorld;
	public static World wildWorld;
	public static World tribalWorld;
	public static World medievalWorld;
	public static World contemporaryWorld;
	public static World futureWorld;
	
	public static boolean DEBBUGING = true;

	@Override
	public void create () {
		camera = new OrthographicCamera(vWidth, vHeight);
		camera.setToOrtho(false, vWidth, vHeight);
		camera.viewportWidth = vWidth;
		camera.viewportHeight = vHeight;

		sRenderer = new ShapeRenderer();
		sRenderer.setProjectionMatrix(camera.combined);
		
		sWidth = Gdx.graphics.getWidth();
		sHeight = Gdx.graphics.getHeight();

		Fonts.init();
		Deployer.init();

		waterWorld = new WaterWorld("Marine").init();
		wildWorld = new WildWorld("Wild").init();
		tribalWorld = new TribalWorld("Tribal").init();
		medievalWorld = new MedievalWorld("Medieval").init();
		contemporaryWorld = new ContemporaryWorld("Contemporary").init();
		futureWorld = new FutureWorld("Future").init();

		DoveCluster.init();
		Dove.init();

		((WaterWorld)waterWorld).initDoves();
		((WildWorld) wildWorld).initDoves();
		((TribalWorld) tribalWorld).initDoves();
		((MedievalWorld) medievalWorld).initDoves();
		((ContemporaryWorld) contemporaryWorld).initDoves();
		((FutureWorld) futureWorld).initDoves();

		((WaterWorld) waterWorld).initDeployers();

		gameBatch = new SpriteBatch();
		gameBatch.setTransformMatrix(camera.combined);

		setWorld(waterWorld);

		bar = new Bar();
		Gui.guiInit();
		Gui.timeMachine.init();
		Gui.phoneGui.init();
		new MoveUpdate().start();
	}

	@Override
	public void render () {
		if(foreground) {
			
			if(Gdx.input.isKeyJustPressed(Keys.E)) 
				currentWorld.doves.add(new Dove(Mouse.getX(), Mouse.getY()).initDove(DoveCluster.clusters[0]));
			
			if(currentWorld != null)
				currentWorld.update(Gdx.graphics.getDeltaTime());
			if(currentGui != null && currentGui.isOpened())
				currentGui.update(Gdx.graphics.getDeltaTime());
			bar.update(Gdx.graphics.getDeltaTime());
		}
	}

	public static void setWorld(World w) {
		currentWorld = w;
	}

	public static World getWorld() {
		return currentWorld;
	}

	public static void out(String s) { System.out.println("[GAME OUT] -> "+s); }
	public static void out(float f) { System.out.println("[GAME OUT] -> "+f); }

	public static class Mouse {
		public static float getX() {
			return getUnp().x;
		}

		public static float getY() {
			return getUnp().y;
		}

		public static Rectangle rect() {
			return new Rectangle(getUnp().x, getUnp().y, 1, 1);
		}

		public static boolean down() {
			return Gdx.input.isTouched();
		}

		public static Vector3 getUnp() {
			Vector3 rtn = new Vector3();
			camera.unproject(rtn.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			return rtn;
		}
	}

	public class MoveUpdate extends Thread {
		public void run() {
			while(true) {
				try {
					if(foreground)
						update();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		@SuppressWarnings("static-access")
		public void update() throws InterruptedException {
			if(currentWorld != null) {
				for(int x = 0 ; x < currentWorld.doves.size() ; x++) {
					currentWorld.doves.get(x).move();
				}
			}
			this.sleep((long) 2000);
		}
	}

	@Override
	public void pause () {
		foreground = false;
	}

	@Override
	public void resume () {
		foreground = true;
	}

	@Override
	public void dispose () {
		Game.out("DISPOSED");
	}
}
