package me.landrade.src;

import me.landrade.src.Game.Mouse;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class PhoneGui extends Gui {
	
	Texture bg;
	CloseButton cButton;
	int anchor = 800;
	boolean closing = false;
	App timeMachine = new App("ngui/buttons/timeMachine.png", 50, 610, Gui.timeMachine);
	App coinStore = new App("ngui/buttons/coinStore.png", 130, 610, Gui.timeMachine);
	App diamondStore = new App("ngui/buttons/diamondStore.png", 210, 610, Gui.timeMachine);
	App settings = new App("ngui/buttons/settings.png", 290, 610, Gui.timeMachine);
	App dovepedia = new App("ngui/buttons/dovepedia.png", 50, 510, Gui.timeMachine);
	App mutations = new App("ngui/buttons/mutations.png", 130, 510, Gui.timeMachine);
	App historyBook = new App("ngui/buttons/historyBook.png", 210, 510, Gui.timeMachine);
	App credits = new App("ngui/buttons/credits.png", 290, 510, Gui.timeMachine);
	
	public void init() {
		batch = Game.getWorld().batch; 
		bg = new Texture("ngui/phone.png");
		cButton = new CloseButton(189, 115);
		timeMachine.init();
		coinStore.init();
		diamondStore.init();
		settings.init();
		dovepedia.init();
		mutations.init();
		historyBook.init();
		credits.init();
	}
	
	public void update(float delta) {
		render(delta);
		if(anchor > 0 && !closing) {
			anchor -= 100;
		} else if(!closing) {
			timeMachine.update(delta);
			coinStore.update(delta);
			diamondStore.update(delta);
			settings.update(delta);
			dovepedia.update(delta);
			mutations.update(delta);
			historyBook.update(delta);
			credits.update(delta);
			cButton.update();
		}
		
		if(closing) {
			if(anchor > -800)
				anchor -= 100;
			else {
				this.opened = false;
				anchor = 800;
				closing = false;
			}
		}
	}

	public void close() {
		closing = true;
	}
	
	public void render(float delta) {
		batch.begin();
		batch.draw(guibg, 0, 0);
		batch.draw(bg, 0, anchor);
		batch.end();
	}
	
	public class App {
		float posX;
		float posY;
		float w;
		float h;
		Sprite s;
		String img;
		Gui target;
		
		public App(String img, float posX, float posY, Gui target) {
			this.posX = posX;
			this.posY = posY;
			this.w = 86;
			this.h = 94;
			this.img = img;
			this.target = target;
		}
		
		public void init() {
			s = new Sprite(new Texture(img));
			s.setSize(w, h);
			s.setPosition(posX, posY);
		}
		
		float add = 0;
		float max = .1f;
		float toadd = 0.02f;
		boolean sadd = false;
		
		public void update(float delta) {
			batch.begin();
			s.draw(batch);
			batch.end();
			
			s.setScale(1 + add);
			
			if(Mouse.rect().overlaps(bounds()))
				sadd = true;
			else
				sadd = false;
			
			if(sadd) {
				if(add < max) {
					add += toadd;
				}
			} else {
				if(add > 0) {
					add -= toadd;
				}
			}
			
			if(Mouse.rect().overlaps(bounds()) && Mouse.down()) {
				target.open();
			}
		}
		
		public Rectangle bounds() {
			return new Rectangle(posX, posY, w, h);
		}
	}
}
