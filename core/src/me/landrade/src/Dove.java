package me.landrade.src;

import java.util.Random;

import me.landrade.src.Game.Mouse;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class Dove extends Entity {

	protected DoveCluster c;
	protected TextureRegion currentTexture;
	protected SpriteBatch b;
	protected boolean alive = true;
	protected int textureIndex = 0;
	protected float velX = 0;
	protected float velY = 0;
	protected float speed = 0.01f;
	protected Sprite sprite;

	public boolean isDragging = false;
	public static Texture doveSheet;
	
	public static void init() {
		doveSheet = new Texture("entities/doveSheet.png");
	}

	public static int textureSize = 128;
	public static TextureRegion doveAt(int indexX, int indexY) {
		return new TextureRegion(doveSheet, (indexX * textureSize), (indexY * textureSize), textureSize, textureSize);
	}

	public Dove(float posX, float posY) {
		this.posX = posX;
		this.posY = posY;
	}

	public Dove initDove(DoveCluster c) {
		this.c = c;
		this.b = c.world.batch;
		currentTexture = doveAt(c.index, textureIndex);
		sprite = new Sprite(currentTexture);
		new Anim().start();
		return this;
	}

	public void setVel(float x, float y) {
		this.velX = x;
		this.velY = y;
	}

	Random rand = new Random();
	public void update(float delta) {
		if(alive) {
			this.posX = posX += velX;
			this.posY = posY += velY;

			if(!isDragging) {
				if(this.posX + this.c.w > Game.vWidth) {
					setVel(0, 0);
					addVel(-3f, 0);
				}

				if(this.posX < 0) {
					setVel(0, 0);
					addVel(3f, 0);
				}

				if(this.posY + this.c.h > Game.vHeight) {
					setVel(0, 0);
					addVel(0, -3f);
				}

				if(this.posY < Game.bar.texture.getHeight() - 17) {
					setVel(0, 0);
					addVel(0, 3f);
				}
			}

			if(velY > 0) velY -= speed;
			if(velY < 0) velY += speed;
			if(velX > 0) velX -= speed;
			if(velX < 0) velX += speed;

			if(Mouse.down() && Mouse.rect().overlaps(bounds()) && !c.world.isAnyDragging()) 
				isDragging = true;
			
			if(!Mouse.down())
				isDragging = false;

			if(isDragging) {
				setPosition(Mouse.getX() - c.w / 2, Mouse.getY() - c.h / 2);
				setVel(0, 0);
			}

			render(delta);
		}
	}
	
	public Dove addVel(float velX, float velY) {
		this.velX += velX;
		this.velY += velY;
		return this;
	}

	
	public void render(float delta) {
		if(alive) {
			currentTexture = doveAt(c.index, textureIndex);
			sprite.setRegion(currentTexture);
			b.begin();
			b.draw(sprite, posX, posY, c.w, c.h);
			b.end();
			if(Game.DEBBUGING) {
				Game.sRenderer.begin(ShapeType.Line);
				Game.sRenderer.setColor(Color.YELLOW);
				Game.sRenderer.rect(fuseBounds().x, fuseBounds().y, fuseBounds().width, fuseBounds().height);
				Game.sRenderer.setColor(Color.BLUE);
				Game.sRenderer.rect(bounds().x, bounds().y, bounds().width, bounds().height);
				Game.sRenderer.end();
			}
		}
	}

	public void setPosition(float posX, float posY) {
		this.posX = posX;
		this.posY = posY;
	}

	public void kill() { alive = false; }

	public Rectangle bounds() {
		return new Rectangle(posX, posY, c.w, c.h);
	}

	float reclusion = 20;
	public Rectangle fuseBounds() {
		return new Rectangle(posX + reclusion, posY + reclusion, c.w - (reclusion * 2), c.h - (reclusion * 2));
	}
	
	public void move() {
		if(!isDragging)
			addVel(1f - rand.nextFloat() * 1.5f, 0.5f - rand.nextFloat() * 1);
	}

	float base = 1.5f;
	public void fuse(Dove d, Dove d2) {
		if(d != d2 && d.c.id == d2.c.id && d.alive && d2.alive && !Mouse.down()) {
			int nextId = d.c.index + d2.c.index;
			if(nextId == 0) nextId = 1;

			if(DoveCluster.clusters[d.c.id].fusable) {
				d.isDragging = false;
				d2.isDragging = false;

				DoveCluster cluster = DoveCluster.clusters[nextId];

				/*if(d.posX > d2.posX) {
					d.velX -= base;
					d2.velX += base;
				}

				if(d.posX < d2.posX) {
					d.velX += base;
					d2.velX -= base;
				}

				if(d.posY > d2.posY) {
					d.velY -= base;
					d2.velY += base;
				}

				if(d.posY < d2.posY) {
					d.velY += base;
					d2.velY -= base;
				}*/
				
				if(d.posY > d2.posY) {
					d.posY -= base;
					d2.posY += base;
				}
				
				if(d.posY < d2.posY) {
					d.posY += base;
					d2.posY -= base;
				}
				
				if(d.posX > d2.posX) {
					d.posX -= base;
					d2.posX += base;
				}
				
				if(d.posX < d2.posX) {
					d.posX += base;
					d2.posX -= base;
				}

				if((d.posX - d2.posX < 10 && d.posX - d2.posX > -10) && (d.posY - d2.posY < 10 && d.posY - d2.posY > -10)) {
					if(!d.c.transport) {
						d.c.world.animations.add(new Explosion(d.posX - d.width, d.posY + 20, Game.waterWorld));
						d.kill();
						d2.kill();
						d.c.world.doves.add(new Dove(d.posX, d.posY).initDove(cluster));
					} else {
						d.kill();
						d2.kill();
						d.c.world.animations.add(new Portal(d.posX, d.posY, Game.waterWorld));
						d.c.tWorld.doves.add(new Dove(d.posX, d.posY).initDove(cluster));
					}
				}
			}
		}
	}

	public class Anim extends Thread {
		public void run() {
			while(alive) {
				try {
					update();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		@SuppressWarnings("static-access")
		public void update() throws InterruptedException {
			if(textureIndex < c.framesNum - 1) {
				textureIndex++;
			} else {
				textureIndex = 0;
			}
			this.sleep((long) c.animSpeed);
			if(!alive) {
				this.interrupt();
			}
		}
	}
}
