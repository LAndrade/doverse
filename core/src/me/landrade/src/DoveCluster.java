package me.landrade.src;

public class DoveCluster {
	
	int index;
	int framesNum;
	int id;
	float animSpeed;
	float w;
	float h;
	boolean fusable;
	World world;
	World tWorld;
	boolean transport;
	String name;
	public static DoveCluster[] clusters;
	
	public static void init() {
		clusters = new DoveCluster[24];
		initDoves();
	}
	
	public static void initDoves() {
		new DoveCluster(0 , "Dove").index(0 ).anim(3, 300).size(95, 95).world(Game.waterWorld).fuse(true);
		new DoveCluster(1 , "Dove").index(1 ).anim(3, 300).size(95, 95).world(Game.waterWorld).fuse(true);
		new DoveCluster(2 , "Dove").index(2 ).anim(3, 300).size(95, 95).world(Game.waterWorld).fuse(true).send(Game.wildWorld);
		new DoveCluster(3 , "Dove").index(3 ).anim(3, 300).size(100, 100).world(Game.wildWorld).fuse(true);
		new DoveCluster(4 , "Dove").index(4 ).anim(3, 300).size(100, 100).world(Game.wildWorld).fuse(true);
		new DoveCluster(5 , "Dove").index(5 ).anim(3, 300).size(100, 100).world(Game.wildWorld).fuse(true).send(Game.tribalWorld);
		new DoveCluster(6 , "Dove").index(6 ).anim(3, 300).size(105, 105).world(Game.tribalWorld).fuse(true);
		new DoveCluster(7 , "Dove").index(7 ).anim(3, 300).size(105, 105).world(Game.tribalWorld).fuse(true);
		new DoveCluster(8 , "Dove").index(8 ).anim(3, 300).size(105, 105).world(Game.tribalWorld).fuse(true).send(Game.medievalWorld);
		new DoveCluster(9 , "Dove").index(9 ).anim(3, 300).size(110, 110).world(Game.medievalWorld).fuse(true);
		new DoveCluster(10, "Dove").index(10).anim(3, 300).size(110, 110).world(Game.medievalWorld).fuse(true);
		new DoveCluster(11, "Dove").index(11).anim(3, 300).size(110, 110).world(Game.medievalWorld).fuse(true).send(Game.contemporaryWorld);
		new DoveCluster(12, "Dove").index(12).anim(3, 300).size(115, 115).world(Game.contemporaryWorld).fuse(true);
		new DoveCluster(13, "Dove").index(13).anim(3, 300).size(115, 115).world(Game.contemporaryWorld).fuse(true).send(Game.futureWorld);
		new DoveCluster(14, "Dove").index(14).anim(3, 300).size(120, 120).world(Game.futureWorld).fuse(true);
		new DoveCluster(15, "Dove").index(15).anim(3, 300).size(120, 120).world(Game.futureWorld).fuse(false);
	}
	
	public DoveCluster(int id, String name) {
		this.name = name;
		this.id = id;
		clusters[id] = this;
	}
	
	public DoveCluster index(int index) {
		this.index = index;
		return this;
	}
	
	public DoveCluster send(World w) {
		transport = true;
		this.tWorld = w;
		return this;
	}
	
	public DoveCluster anim(int framesNum, float animSpeed) {
		this.framesNum = framesNum;
		this.animSpeed = animSpeed;
		return this;
	}
	
	public DoveCluster fuse(boolean fusable) {
		this.fusable = fusable;
		return this;
	}
	
	public DoveCluster world(World world) {
		this.world = world;
		return this;
	}
	
	public DoveCluster size(float w, float h) {
		this.w = w;
		this.h = h;
		return this;
	}
}
