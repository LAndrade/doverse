package me.landrade.src;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class Deployer extends Entity {

	protected TextureRegion currentTexture;
	protected SpriteBatch b;
	protected int textureIndex = 0;
	protected World w;
	protected float width = 80;
	protected float height = 80;
	protected Anim anim;
	protected Random rand = new Random();

	public static Texture sheet;

	public static void init() {
		sheet = new Texture("entities/deployer.png");
	}

	public static int textureSize = 256;
	public static TextureRegion doveAt(int indexX) {
		return new TextureRegion(sheet, (indexX * textureSize), 0, textureSize, textureSize);
	}

	public Deployer(World w, float posX, float posY) {
		this.w = w;
		this.posX = posX;
		this.posY = posY;
	}

	public Deployer initDeployer() {
		this.b = w.batch;
		currentTexture = doveAt(0);
		anim = new Anim();
		anim.start();
		return this;
	}

	boolean trigger = false;
	public void update(float delta) {
		currentTexture = doveAt(textureIndex);
		render(delta);
	}

	public void render(float delta) {
		b.begin();
		b.draw(currentTexture, posX, posY, width, height);
		b.end();
		if(Game.DEBBUGING) {
			Game.sRenderer.begin(ShapeType.Line);
			Game.sRenderer.setColor(Color.RED);
			Game.sRenderer.rect(bounds().x, bounds().y, bounds().width, bounds().height);
			Game.sRenderer.end();
		}
	}

	public void setPosition(float posX, float posY) {
		this.posX = posX;
		this.posY = posY;
	}

	public Rectangle bounds() {
		return new Rectangle(posX, posY, width, height);
	}

	public class Anim extends Thread {
		boolean running = false;
		public void run() {
			while(true) {
				try {
					update();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		public void deploy() {
			if(w.getLivingDoves() < w.limit)
				running = true;
		}

		@SuppressWarnings("static-access")
		public void update() throws InterruptedException {
			if(running) {
				if(textureIndex < 3) {
					textureIndex++;
					this.sleep(100);
				} else {
					textureIndex = 0;
					w.doves.add(new Dove(posX, posY + height - 20).initDove(DoveCluster.clusters[0]).addVel(0, 0.2f + rand.nextFloat() * 1.5f));
					running = false;
				}
			}
		}
	}
}
